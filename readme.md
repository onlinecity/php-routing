# php-routing

#### Elegant and unobtrusive router  

It is designed to be intuitive, unobtrusive, and let you do more with less code.

The Router does route resolving without regular expressions. The UrlMatcher which is a dependant component of the Router matches routes against the detected or specified url, as seen in the examples below. The UrlMatcher's objective is to be fast and return as early as possible and get any other route from the router to match.

To parse urls and path values the Router uses this [Request](https://github.com/onlinecity/php-http/blob/master/src/Request.php) class. It supports autodetection of url, paths etc. and allows you to define your own url and request method.

## Features

 - Middleware support
 - Automatic controller + action dispatching: _controller/_action
 - Callables as named functions or closures with support for named parameters.
 - Restful support using verbs in controller methods: getUser, putUser (see example)
 - Named parameters, index.php/demo/{b}/{a} gets reversed arguments.
 - Patterns with regular expression, /index.php/^hello[a-z]+$/get-list/
 - Provide URL or autodetect from http request by using Request internally.
 - Provide [Request](https://github.com/onlinecity/php-http/blob/master/src/Request.php) instance with createFromRequest method

## Todo

 - Optimize Routegroup to build namespace only when requested by the Router
 - Add pattern argument to Routegroup, new Routegroup($pattern, $namespace=null)
 - Supports regular expressions in named parameters: {id:\d+...}

## Installation

	composer require onlinecity/php-routing:dev-master

Your `composer.json` would look something like this

```
{
  "name": "demo",
  "require": {
    "php": ">=5.3.0",
    "onlinecity/php-http": "dev-master"
  }
}
```

## Tests

The router is very testable. See tests/ folder. You simply run the tests with phpunit.
On OSX you could simply issue a `brew install phpunit` command to install phpunit and then `phpunit` to run the testsuite. The composer.json file has you covered with autoloading of the 'tests' namespace.

## Example

In this example our goal is to route RESTful requests to our \Admin\UserController.
We have defined four methods in our controller, notice the verb's "get", "put", "delete".

Example controller source `src/Admin/Controllers/UserController.php`

```
<?php namespace Admin\Controllers;

class UserController
{
	public function index() { // show default }
	public function getUser($id)
	{
		// return user by id
		return array('id'=>$id, 'name'=>'john doe');
	}
	public function deleteUser($id)
	{
		// delete user by id
		$this->service->deleteUser($id);
	}
	public function putUser($id)
	{
		// update user by id
		// as an example, \Onlinecity\Http\Request is used to retrieve REST data
		$this->service->updateUser($id, array(
			'name' => $this->request->getData('name')
		);
	}
}
```

We then need the Router to dispatch requests. In this example we don't need to define any Routes but will be using the routers automatic controller logic to resolve the url.

If no routes are added to the router a default route is automatically added with the default pattern `_controller/_action`. The default pattern resolves url values into namespace and controllers and methods on the resolved controller. The router automatically detects RESTful requests and tries to match http-method in lowercased verbs as seen in the example.

Router setup is quite simple as seen in the following example.

```
<?php
$router = Router::createDefault();
$router->dispatch();
```

Now we should be able to request methods in our RESTful controller.

	HTTP-GET
	http://localhost/demo/index.php/admin/user/index
	=> index()

	HTTP-GET
	http://localhost/demo/index.php/admin/user/user/1
	=> getUser($id)

	HTTP-DELETE
	http://localhost/demo/index.php/admin/user/user/1
	=> deleteUser($id)

	HTTP-PUT + AJAX + JSON
	http://localhost/demo/index.php/admin/user/user/1
	=> putUser($id)

If we where to create a testable version of our example code, we need to tell the router what our url looks like. That is obviously also quite simple as you might have guessed.


```
<?php
$router = Router::createFromUrl('http://localhost/demo/index.php/admin/user/1', 'get');
$user = $router->dispatch();

// we should now have the user array returned from our RESTful controller
if (isset($user['id']) && $user['id'] == 1) {
	exit('success, we got the user');
}
```

Another example is to use [Request](https://github.com/onlinecity/php-http/blob/master/src/Request.php)  as request handler. In our example controller we have used the Request class to facilitate handling of http requests with a minimum of code.

```
<?php

$request = new Request();
$router = Router::createFromRequest($request);
$user = $router->dispatch();
...
```

## License

Copyright (c) 2014 Jari Berg, OnlineCity ApS

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
