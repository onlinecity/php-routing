<?php namespace Onlinecity\Routing;

/**
 * Route
 *
 * Elegant and unobtrusive route with middleware support.
 *
 * @see Router.php
 *
 * @package Onlinecity\Routing
 * @author Jari Berg <jb@onlinecity.dk>
 */
class Route
{
  /**
   * If specified, the callable invoked when dispatch is called
   * @var callable
   */
  protected $callable;

  /**
   * Middlewares
   * @var array of callables
   */
  protected $middlewares;

  /**
   * If specified, the callable receives these parameters when invoked
   * @var array
   */
  protected $parameters;

  /**
   * Pattern
   * @var string
   */
  protected $pattern;

  /**
   * Namespace
   * @var string
   */
  protected $namespace;

  /**
   * Constructor
   *
   * Defaults to _controller/_action which resolves:
   *
   * http://host/index.php/demo.demo/list/1 => \Demo\DemoController\listAction(1)
   *
   * @param string $pattern [optional]
   * @param callable $callable [optional]
   */
  public function __construct($pattern = '_controller/_action', callable $callable = null)
  {
    $this->setPattern($pattern);
    if (is_callable($callable)) {
      $this->setCallable($callable);
    }
  }

  /**
   * Dispatch middlewares and route
   *
   * Middleware callables are invoked in the order specified
   *
   * @return mixed|false Returns result from callable and false if no callable
   */
  public function dispatch()
  {
    foreach ((array)$this->middlewares as $middleware) {
      call_user_func_array($middleware, array($this));
    }
    if (is_callable($this->getCallable())) {
      return call_user_func_array($this->getCallable(), array_values($this->getParameters()));
    } else {
      return false;
    }
  }

  /**
   * Get specified callable which is invoked when dispatch is called
   *
   * @return callable
   */
  public function getCallable()
  {
    return $this->callable;
  }

  /**
   * Middleware callables
   *
   * @return array of middleware callables
   */
  public function getMiddlewares()
  {
    return (array)$this->middlewares;
  }

  /**
   * Get specified namespace
   *
   * @return string
   */
  public function getNamespace()
  {
    return $this->namespace;
  }

  /**
   * Get specified parameters
   * @return array
   */
  public function getParameters()
  {
    return (array)$this->parameters;
  }

  /**
   * Get pattern which is used by Router when matching routes
   * @return string
   */
  public function getPattern()
  {
    return $this->pattern;
  }

  /**
   * Callable to invoke when dispatch is called
   *
   * @param $callable Callable to be invoked when dispatch is called
   *
   * @return Route
   */
  public function setCallable($callable = null)
  {
    $this->callable = $callable;
  }

  /**
   * Set middlewares
   *
   * @throws \InvalidArgumentException
   *
   * @param array $callables Array of callables
   *
   * @return Route
   */
  public function setMiddlewares(array $callables)
  {
    $this->middlewares = array();
    foreach ($callables as $callable) {
      if (!is_callable($callable)) {
        throw new \InvalidArgumentException('Router middleware must be callable');
      }
      $this->middlewares[] = $callable;
    }
  }

  /**
   * Set namespace
   *
   * @param string $namespace
   *
   * @return Route
   */
  public function setNamespace($namespace)
  {
    $this->namespace = $namespace;
  }

  /**
   * Set parameters
   *
   * @param array $parameters
   *
   * @return Route
   */
  public function setParameters(array $parameters)
  {
    $this->parameters = (array)$parameters;
  }

  /**
   * Set pattern which is used by Router when matching routes
   * Supports _controller and _action shorthands which is resolved by Router to \SomeNamespace\SomeController\someAction(...)
   *
   * @param string $pattern
   *
   * @return Route
   */
  public function setPattern($pattern)
  {
    $this->pattern = $pattern;
  }
}