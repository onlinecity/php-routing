<?php namespace Onlinecity\Routing;

use Onlinecity\Http\Request;
use Onlinecity\Routing\Exceptions\DispatchException;
use Onlinecity\Routing\Exceptions\InvalidRouteException;
use Onlinecity\Routing\Exceptions\InvalidUrlException;

/**
 * Router
 *
 * Elegant and unobtrusive router.
 *
 * - Middleware support
 * - Automatic controller + action dispatching with support for named parameters.
 * - Pattern regexp is supported but not required: /index.php/^[a-z]+$/get-list/
 * - Named parameters regexp is supported but not required: {id:\d+...}
 *
 * @package Onlinecity\Routing
 * @author Jari Berg <jb@onlinecity.dk>
 */
class Router
{
  /**
   * Routes
   * @var array of Route
   */
  protected $routes = array();

  /**
   * @var Request
   */
  protected $request;

  /**
   * Constructor
   */
  protected function __construct()
  {
  }

  /**
   * Create default Router using http request autodetection
   *
   * @return Router
   */
  public static function createDefault()
  {
    return new Router();
  }

  /**
   * Create Router from url
   *
   * @param string $url The url to match against, leave empty to autodetect
   * @param string $httpMethod [optional] Option to override http method (GET, PUT, UDPATE, DELETE), leave empty to autodetect
   *
   * @return Router
   */
  public static function createFromUrl($url, $httpMethod = null)
  {
    $router = new Router();
    $router->request = new Request($url, $httpMethod);
    return $router;
  }

  /**
   * Create Router from Request
   *
   * @param Request $request
   *
   * @return Router
   */
  public static function createFromRequest(Request $request)
  {
    $router = new Router();
    $router->request = $request;
    return $router;
  }

  /**
   * Dispatch matched route
   *
   * If no routes  been added a default route is created
   * having the default _controller + _action pattern.
   *
   * Routes are invoked in the order
   * specified by setRoutes and addRoute
   *
   * @throws InvalidRouteException
   * @throws InvalidUrlException
   * @throws DispatchException
   *
   * @return mixed
   */
  public function dispatch()
  {
    if (!$this->request) {
      $this->request = new Request();
    }

    $paths = $this->request->getPathArray();

    if (empty($paths) || current($paths) == '') {
      throw new InvalidUrlException('Invalid url is without pathinfo: '.$this->request->getUrl());
    }

    $matcher = new UrlMatcher($paths, $this->request->getMethod());

    if (empty($this->routes)) {
      $this->routes = array(new Route());
    }

    foreach ($this->routes as $route) {
      /* @var $route Route */
      if (false === $route instanceof Route) {
        throw new InvalidRouteException('Route must be an instance of ' . get_class(new Route()));
      }
      if ($matchedRoute = $matcher->match($route)) {
        $this->routes = array();
        return $matchedRoute->dispatch();
      }
    }

    throw new DispatchException('No route matched url: ' . $this->request->getUrl());
  }

  /**
   * Add route
   *
   * @param Route $route
   */
  public function addRoute(Route $route)
  {
    $this->routes[] = $route;
  }

  /**
   * Add routes
   *
   * @param array $routes
   */
  public function addRoutes(array $routes)
  {
    $this->routes = array_merge($this->routes, (array)$routes);
  }

  /**
   * Set Routegroup
   *
   * @param Routegroup $routegroup
   */
  public function addRoutegroup(Routegroup $routegroup)
  {
    $this->addRoutes($routegroup->getRoutes());
  }
}
