<?php namespace Onlinecity\Routing;

/**
 * Routegroup
 *
 * Elegant and unobtrusive routegroup.
 *
 * @see Router.php
 *
 * @package Onlinecity\Routing
 * @author Jari Berg <jb@onlinecity.dk>
 */
class Routegroup
{
  /**
   * Routes
   * @var array of Route
   */
  protected $routes;

  /**
   * Namespace
   * @var string
   */
  protected $namespace;

  /**
   * Constructor
   *
   * @param string $path
   */
  public function __construct($path = null)
  {
    if (strpos($path, '\\') === false) {
      $this->namespace = '\\' . str_replace(' ', '', ucwords(str_replace('/', ' \\ ', trim($path, '/'))));
    } else {
      $this->namespace = $path;
    }
    $this->routes = array();
  }

  /**
   * Add Route
   *
   * @param Route $route
   */
  public function addRoute(Route $route)
  {
    if (!$route->getNamespace()) {
      $route->setNamespace($this->namespace);
    }
    $this->routes[] = $route;
  }

  /**
   * Add routes
   *
   * @param array $routes
   */
  public function addRoutes(array $routes)
  {
    foreach ($routes as $route) {
      $this->addRoute($route);
    }
  }

  /**
   * Get routes
   * @return array
   */
  public function getRoutes()
  {
    if (empty($this->routes)) {
      $route = new Route();
      $route->setNamespace($this->namespace);
      return array($route);
    } else {
      return $this->routes;
    }
  }
}