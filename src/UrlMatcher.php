<?php namespace Onlinecity\Routing;

use Onlinecity\Http\Request;

/**
 * UrlMatcher
 *
 * Elegant and unobtrusive url matcher.
 *
 * This url matcher is very fast, clean
 * and without using regular expressions.
 *
 * @see Router.php
 *
 * @package Onlinecity\Routing
 * @author Jari Berg <jb@onlinecity.dk>
 */
class UrlMatcher
{
  /**
   * Paths from url
   * /user/list-users => ['user', 'list-users']
   * @var array
   */
  protected $paths;

  /**
   * RESTful verb (get, put, update, delete)
   * Lowercase variant of http method which is part of the controller
   * method name (getUser, putUser, ...) for RESTful controller methods
   *
   * @var string
   */
  protected $verb;

  /**
   * Constructor
   *
   * @param array $paths The pathinfo values that should be matched with a Route
   * @param string $httpMethod [optional] The http method (GET, PUT, UDPATE, DELETE), leave empty to autodetect
   */
  public function __construct($paths, $httpMethod = null)
  {
    $this->paths = $paths;

    // if defined RESTful verb is lowercased request-method string
    $this->verb = $httpMethod ? strtolower($httpMethod) : null;
  }

  /**
   * Get controller object from path
   *
   * @param string $path
   * @param string $namespace[optional]
   *
   * @return \stdClass
   */
  protected function getControllerObject($path, $namespace = null)
  {
    $paths = null;
    $controller = null;
    $qualified = (string) $namespace;
    while (true) {
      $qualified .= '\\' . ucfirst($path);
      $class = $qualified . 'Controller';
      if (class_exists($class)) {
        return new $class;
      }
      $paths = $paths ? : $this->paths;
      $path = array_shift($this->paths);
      if (empty($this->paths)) {
        break;
      }
    }
    $this->paths = $paths;
    return null;
  }

  /**
   * Get controller method in controller from path
   *
   * @param \stdClass $controller
   * @param string $path
   *
   * @return callable|null Return callable or null if no matching method was found
   */
  protected function getControllerCallable($controller, $path)
  {
    if (method_exists($controller, $qualified = '__invoke')) {
      return array($controller, $qualified);
    }

    if (strpos($path, '-') !== false) {
      $action = str_replace(' ', '', lcfirst(ucwords(str_replace('-', ' ', $path))));
    } else {
      $action = $path;
    }

    if (method_exists($controller, $qualified = $action)) {
      return array($controller, $qualified);
    }

    if ($this->verb && method_exists($controller, $qualified = $this->verb . ucfirst($action))) {
      return array($controller, $qualified);
    }

    return null;
  }

  /**
   * Match route with url using no regular expressions
   *
   * @param Route $route
   *
   * @return Route|null The matched route or null if no match
   */
  public function match(Route $route)
  {
    $patterns = explode('/', $route->getPattern());
    $paths = array();
    $parameters = array();
    $parameterNames = array();
    $parameterValues = array();

    $callable = $route->getCallable();
    $controller = null;

    foreach ($patterns as $pattern) {

      $path = array_shift($this->paths);

      // should we detect controller instance?
      if ($pattern == '_controller') {
        $controller = $this->getControllerObject($path, $route->getNamespace());
        continue;
      }

      // should we detect controller method?
      if ($pattern == '_action' && $controller) {
        $callable = $this->getControllerCallable($controller, $path);
        continue;
      }

      $char = substr($pattern, 0, 1);

      // should we process a parameter?
      if ($char == '{') {
        $parameterNames[] = trim($pattern, '{}');
        $parameterValues[] = $path;
        continue;
      }

      // very fast check if we should use regular expression matching?
      // you should not have to use regular expressions if you use the default
      // _controller/_action automatic controller logic or provide callables
      if ($this->isRegExp(null, $char)) {
        if (preg_match('@' . $pattern . '@i', $path)) {
          $paths[] = $path;
          continue;
        } else {
          return null;
        }
      }

      // normal compare first by length (10 x faster than string compare)
      if (mb_strlen($pattern) == mb_strlen($path) && mb_strtolower($pattern) == mb_strtolower($path)) {
        $paths[] = $path;
        continue;
      }

      return null;
    }

    // named parameters is obviously supported but in my mind exaggerated and not really required.
    $callableParameters = array();

    // if pattern contains named parameters {} we get parameters from our callable by reflection
    if (empty($parameterNames) === false && is_callable($callable)) {
      if (is_array($callable)) {
        $method = new \ReflectionMethod(get_class($callable[0]), $callable[1]);
      } else {
        $method = new \ReflectionFunction($callable);
      }
      foreach ($method->getParameters() as $parameter) {
        $callableParameters[$parameter->getName()] = $parameter->isDefaultValueAvailable() ? $parameter->getDefaultValue() : null;
      }
    }

    // map named parameters to callable parameters in the order they are specified in the pattern
    foreach ($callableParameters as $callableParameterName => $callableParameterDefault) {
      $pos = array_search($callableParameterName, $parameterNames);
      if ($pos !== false && $parameterValues[$pos] !== null) {
        $parameters[] = $parameterValues[$pos];
      } else {
        $parameters[] = $callableParameterDefault;
      }
    }

    if (!$callable || !is_callable($callable)) {
      return null;
    }

    $matched = clone($route);

    if (false == empty($this->paths)) {
      $paths = array_merge($paths, $this->paths);
    }

    $parameters = array_merge($parameters, $paths);

    $matched->setCallable($callable);
    $matched->setParameters($parameters);

    return $matched;
  }

  /**
   * Fast test if string or char is a regular expression
   *
   * @param string $string
   * @param string $char[optional]
   *
   * @return bool
   */
  protected function isRegExp($string, $char='')
  {
    switch ($char ?: mb_substr($string, 0, 1)) {
      case '^':
      case '[':
      case '\\':
      case '.':
        return true;
      default:
        return false;
    }
  }
}
