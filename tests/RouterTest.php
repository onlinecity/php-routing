<?php namespace Onlinecity\Routing\Tests;

use Onlinecity\Routing\Exceptions\DispatchException;
use Onlinecity\Routing\Exceptions\InvalidUrlException;
use Onlinecity\Routing\Route;
use Onlinecity\Routing\Routegroup;
use Onlinecity\Routing\Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
  public function testAutomaticRoute()
  {
    // Using Onlinecity\Routing\Tests\UserController
    $router = Router::createFromUrl('http://localhost/index.php/onlinecity/routing/tests/user/index');
    $this->assertEquals('index', $router->dispatch());
  }

  public function testAutomaticRouteCamelcase()
  {
    // Using Onlinecity\Routing\Tests\UserController
    // test actual http request
    $_SERVER['REQUEST_URI'] = 'onlinecity/routing/tests/user/list-users?dummy';
    $router = Router::createDefault();
    $this->assertEquals(array(1, 2, 3), $router->dispatch());
  }

  public function testAutomaticRouteWithParameter()
  {
    // Using Onlinecity\Routing\Tests\UserController
    $router = Router::createFromUrl('http://localhost/index.php/onlinecity/routing/tests/user/get-user/1');
    $this->assertEquals(1, $router->dispatch());
  }

  public function testRouteWithClosure()
  {
    $self = $this;
    $route = new Route('test/{b}/{a}', function ($a = 'a', $b = 'b', $c = 'c') use ($self) {
      $self->assertEquals('a', $a);
      $self->assertEquals('1', $b);
      $self->assertEquals('c', $c);
    });
    $router = Router::createFromUrl('http://localhost/index.php/test/1');
    $router->addRoutes(array($route));
    $router->dispatch();
  }

  public function testAutomaticRouteRestPut()
  {
    // Using Onlinecity\Routing\Tests\UserController
    $routegroup = new Routegroup('onlinecity/routing/tests');
    $router = Router::createFromUrl('http://localhost/index.php/user/user/1/john/doe', 'put');
    $router->addRoutegroup($routegroup);
    $user = $router->dispatch();
    $this->assertEquals(1, $user->id);
    $this->assertEquals('john', $user->name);
    $this->assertEquals('doe', $user->surname);
  }

  public function testInvalidUrl()
  {
    $this->setExpectedException(get_class(new InvalidUrlException()));
    $router = Router::createFromUrl('http://localhost');
    $router->dispatch();
  }

  public function testInvalidRoute()
  {
    $_SERVER['REQUEST_URI'] = 'http://localhost/invalid-route';
    $this->setExpectedException(get_class(new DispatchException()));
    $router = Router::createDefault();
    $router->dispatch();
  }
}