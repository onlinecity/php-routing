<?php namespace Onlinecity\Routing\Tests;

class UserController
{
  public function test($a = 'a', $b = 'b', $c = 'c')
  {
    return array($a,$b,$c);
  }

  public function index()
  {
    return 'index';
  }

  public function getUser($id)
  {
    return $id;
  }

  public function listUsers()
  {
    return array(1, 2, 3);
  }

  public function postUser()
  {
  }

  public function putUser($id, $name, $surname)
  {
    $user = array('id' => $id, 'name' => $name, 'surname' => $surname);
    return (object) $user;
  }

  public function deleteUser()
  {
  }
}